package com.chanatip.week5;

public class BubbleSortApp {
    public static void swap(int[] arr, int frist, int second) {
        int temp = arr[frist];
        arr[frist] = arr[second];
        arr[second] = temp;
    }

    public static void bubble(int[] arr, int frist, int second) {
        for(int i = frist; i<second; i++){
            if(arr[i]>arr[i+1]){
                swap(arr,i ,i+1);
            }
        }
    }

    public static void bubbleSort(int[] arr) {
        for(int i=arr.length-1; i >0; i--){
            bubble(arr, 0, i);

        }
    }

}
