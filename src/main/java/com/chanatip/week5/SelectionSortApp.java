package com.chanatip.week5;

public class SelectionSortApp {
    public static void main(String[] args) {
        int arr[] = new int[1000];
        RandomArray(arr);
        print(arr);
        selecttionsort(arr);
        print(arr);
    }
    public static void print(int[] arr){
        for(int a: arr){
            System.out.print(a + " ");
        }
        System.out.println();
    }
    public static void RandomArray(int arr[]){
        for(int i = 0; i< arr.length; i++){
            arr[i] = (int)(Math.random()*10000);
         }
    }
    public static int findMinIndex(int[] arr, int pos) {
        int minIdex = pos;
        for(int i = pos+1; i<arr.length; i++){
            if(arr[minIdex]>arr[i]){
                minIdex = i;
            }
        }
        return minIdex;
    }

    public static void swap(int[] arr, int frist, int second) {
        int temp = arr[frist];
        arr[frist] = arr[second];
        arr[second] = temp;
    }

    public static void selecttionsort(int[] arr) {
        int minIdex;
        for(int pos = 0; pos<arr.length-1;pos++){
            minIdex = findMinIndex(arr, pos);
            swap(arr, minIdex, pos);
        }
       
    }
}
